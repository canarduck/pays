"""
Tests unitaires
"""
import unittest
import pays


class InitTestCase(unittest.TestCase):
    """
    Tests de l'instanciation de Countries
    """

    def test_init_empty(self):
        "init sans paramètres = fra"
        countries = pays.Countries()
        self.assertEqual(countries.locale, 'fra')

    def test_init_locale(self):
        "locale spécifiée"
        countries = pays.Countries('ita')
        self.assertEqual(countries.locale, 'ita')

    def test_init_exclude(self):
        "exclude spécifié"
        countries = pays.Countries(exclude=['ES'])
        with self.assertRaises(KeyError):
            countries.get('ES')
        self.assertIsInstance(countries.get('DE'), pays.Country)

    def test_init_only(self):
        "only spécifié"
        countries = pays.Countries('eng', only=['ES'])
        with self.assertRaises(KeyError):
            countries.get('DE')
        self.assertIsInstance(countries.get('ES'), pays.Country)

    def test_init_only_and_exclude(self):
        "only et exclude spécifiés"
        with self.assertRaises(AttributeError):
            pays.Countries(only=['ES'], exclude=['DE'])

    def test_init_exclude_not_list(self):
        "exclude spécifié mais pas en liste"
        with self.assertRaises(AttributeError):
            pays.Countries('deu', exclude='ES')

    def test_init_only_not_list(self):
        "only spécifié mais pas en liste"
        with self.assertRaises(AttributeError):
            pays.Countries('cym', only='ES')


class GetTestCase(unittest.TestCase):
    """
    Tests de récupération d’un pays
    """

    def setUp(self):
        self.countries = pays.Countries()

    def test_get_default(self):
        "get retourne un Country"
        country = self.countries.get('FR')
        self.assertIsInstance(country, pays.Country)

    def test_get_lower_alpha(self):
        "get marche avec alpha2 en lowercase"
        country1 = self.countries.get('fr')
        country2 = self.countries.get('FR')
        self.assertEqual(country1.ccn3, country2.ccn3)

    def test_get_fail(self):
        "get raise error si inconnu"
        with self.assertRaises(KeyError):
            self.countries.get('XX')

    def test_get_none_fail(self):
        "get raise error si invalide"
        with self.assertRaises(KeyError):
            self.countries.get(None)

    def test_get_alpha2(self):
        "get marche avec alpha2"
        country = self.countries.get('DE')
        self.assertEqual(country.cca2, 'DE')

    def test_get_alpha3(self):
        "get marche avec alpha3"
        country = self.countries.get('ESP')
        self.assertEqual(country.cca2, 'ES')

    def test_get_number(self):
        "get marche avec numéro"
        country = self.countries.get(304)
        self.assertEqual(country.cca2, 'GL')


class IterTestCase(unittest.TestCase):
    """
    Tests de l'utilisation de Countries en dict / générateur
    """

    def setUp(self):
        self.countries = pays.Countries()

    def test_iter(self):
        "itération"
        for country in self.countries:
            self.assertIsInstance(country, pays.Country)

    def test_items(self):
        "dictionnaire"
        for cca2, data in self.countries.countries.items():
            self.assertEqual(cca2, data['cca2'])


class CountryTestCase(unittest.TestCase):
    """
    Tests de l’objet country
    """

    def setUp(self):
        self.countries = pays.Countries()

    def test_str(self):
        "str retourne name"
        country = self.countries.get('FR')
        self.assertEqual(str(country), country.name)

    def test_empty(self):
        "sans data raise exception"
        with self.assertRaises(AttributeError):
            pays.Country(data=None)
