# Journal des modifications

Ce fichier est généré automatiquement après chaque modification du dépôt. 
Les numéros indiqués correspondent aux [versions taggées](https://gitlab.com/canarduck/pays/tags).

## v0.2.3 (2018-02-22)

### ⚔ Correctifs

* Erreur get vide.
* Capture exception int.

## v0.2.2 (2018-02-17)

### ⚔ Correctifs

* Multiples instanciations de countries et data[locale]

## v0.2.1 (2018-02-15)

### ⚔ Correctifs

* Emplacement des datas pour y accéder après pip.

## v0.2.0 (2018-02-15)

### ✍ Changements

* Refacto api privée Countries.

## v0.1.3 (2018-02-15)

### ★ Nouveautés

* Generateur de country.

### ✍ Changements

* Mise à jour de la documentation.

## v0.1.2 (2018-02-14)

### ✍ Changements

* Renommage des classes en anglais.

### ⚔ Correctifs

* Génération eng.json.

## v0.1.0 (2018-02-14)

### ★ Nouveautés

* Génération des fichiers et interface de base.

### ⚔ Correctifs

* Dev-requirements.

