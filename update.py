#!/usr/bin/env python3
"""
Lanceur pour la mise à jour des données

`python update.py`
"""
import os
import json


def update_data():
    """
    Les données de **pays** proviennent de https://mledoze.github.io/countries et sont liées dans un sous module git.
    `update_data()` transforme cette source en fichiers, un par langue, au format json.
    Note : cette fonction n'est utilisée que pendant le développement, le paquet pip contient déjà ces données.
    """
    files = {'eng': {}}
    try:
        with open('vendor/countries/dist/countries.json') as json_data:
            countries = json.load(json_data)
    except FileNotFoundError:  # Bien penser à récupérer le sous dépôt !
        raise FileNotFoundError(
            'Avez-vous récupéré le dépôt "countries" ? git submodule update ?')
    for country in countries:
        data = {
            'name': country['name']['common'],
            'official_name': country['name']['official'],
            'cca2': country['cca2'],
            'cca3': country['cca3'],
            'ccn3': country['ccn3'],
            'flag': country['flag']
        }
        files['eng'][country['cca2']] = data
        for locale, translation in country['translations'].items():
            if not files.get(locale):
                files[locale] = {}
            data = {
                'name': translation['common'],
                'official_name': translation['official'],
                'cca2': country['cca2'],
                'cca3': country['cca3'],
                'ccn3': country['ccn3'],
                'flag': country['flag']
            }
            files[locale][country['cca2']] = data
    for locale, data in files.items():
        # les fichiers sont stockés dans data/LOCALE.json
        filename = 'pays/data/{locale}.json'.format(locale=locale)
        with open(filename, 'w') as output:
            output.write(json.dumps(data))


if __name__ == '__main__':
    update_data()
